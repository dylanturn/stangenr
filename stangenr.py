import os
import sys
import requests
import thread
import time
import urllib2
import string
from subprocess import call

class setup_info:
    def __init__(self, args):
        self.cluster_name = ""
        self.cluster_host_list = []
        self.master = ""
        self.control = ""
        self.multi_master = ""
        self.data = ""
        self.parse_arg(args)

    def parse_arg(self, args):
        for i in range(0,len(args)):
            if args[i] == "-clustername":
                self.cluster_name = args[i+1]
            if args[i] == "-master":
                self.master = args[i+1]
            if args[i] == "-control":
                self.control = args[i+1]
            if args[i] == "-multimaster":
                self.multi_master = args[i+1] 
            if args[i] == "-data":
                self.data = args[i+1]

masterServer = ""

# {maprCoreVersion}
maprCoreVersion = "5.2.0"
# {licenseType}
maprLicenseType = "M3"
# {mepVersion}
maprMepVersion = "2.0"

# {clusterName}
clusterName = ""
# {clusterHostList}
clusterHostList = ""

# {hostSshUsername}
hostSshUsername = "root"
# {hostSshPassword}
hostSshPassword = "password"
# {hostSshPort}
hostSshPort = "22"
# {diskList}
hostDiskList = "    - {}".format("/dev/sdd")

# {hiveDbHost}
hiveDbHost = "10.210.26.56"
# {hiveSchemaName}
hiveSchemaName = "DylanHiveDB"
# {hiveUsername}
hiveUsername = "34852_admin"
# {hivePassword}
hivePassword = "password"

setup_args = setup_info(sys.argv)

groupControl = ""
groupMaster = ""
groupMultiMaster = ""
groupData = ""
groupDefault = ""

groupDefault += "    - {}\n".format(setup_args.master)
groupDefault += "    - {}\n".format(setup_args.control)
groupDefault += "    - {}\n".format(setup_args.multi_master)
groupDefault += "    - {}\n".format(setup_args.data)

groupControl += "    - {}\n".format(setup_args.control)
groupMaster += "    - {}\n".format(setup_args.master)
groupMultiMaster += "    - {}\n".format(setup_args.multi_master)
groupData += "    - {}\n".format(setup_args.data)

clusterName = setup_args.cluster_name
clusterHostList = groupDefault
masterServer = setup_args.master
    
url = 'https://raw.githubusercontent.com/dylanturn/hadoopprov/master/hadoop-mapr/hadoopsetup.yaml'
configTemplate = urllib2.urlopen(url).read()

configTemplate = configTemplate.replace("{maprCoreVersion}",maprCoreVersion)
configTemplate = configTemplate.replace("{licenseType}",maprLicenseType)
configTemplate = configTemplate.replace("{mepVersion}",maprMepVersion)

configTemplate = configTemplate.replace("{clusterName}",clusterName)

configTemplate = configTemplate.replace("{clusterHostList}",clusterHostList)
configTemplate = configTemplate.replace("{hostSshUsername}",hostSshUsername)
configTemplate = configTemplate.replace("{hostSshPassword}",hostSshPassword)
configTemplate = configTemplate.replace("{hostSshPort}",hostSshPort)

configTemplate = configTemplate.replace("{diskList}",hostDiskList)

configTemplate = configTemplate.replace("{hiveSchemaName}",hiveSchemaName)
configTemplate = configTemplate.replace("{hiveUsername}",hiveUsername)
configTemplate = configTemplate.replace("{hivePassword}",hivePassword)
configTemplate = configTemplate.replace("{hiveDbHost}",hiveDbHost)

configTemplate = configTemplate.replace("{groupDefault}",groupDefault)
configTemplate = configTemplate.replace("{groupMaster}",groupMaster)
configTemplate = configTemplate.replace("{groupMultiMaster}",groupMultiMaster)
configTemplate = configTemplate.replace("{groupData}",groupData)
configTemplate = configTemplate.replace("{groupControl}",groupControl)


# Create the config yaml file.
f = open('/tmp/clusterconfig.yaml', 'w')
f.write(configTemplate)
f.close()